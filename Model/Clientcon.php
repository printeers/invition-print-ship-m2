<?php 

namespace Invition\InvitionPrintShipM2\Model;

use Invition\InvitionPrintShipM2\Api\Data\ItemsInterface;
use Magento\Framework\DataObject\IdentityInterface;
use \Invition\Partnerclient;


class Clientcon extends \Magento\Framework\Model\AbstractModel 
{
 	private $_objectManager;
	private $_partnercode;
	private $_apikey;
	private $_env;
	private $_invitionsku;
	private $_statusordercommitted;
	private $_client;
		
	protected $_resource;
	private $_orderRepository;
	private $_defaultshippingmethod;
	private $_shippingmethodsettings;
	
	
    /**
     * Initialize resource model
     *
     * @return void
     */
   	public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Framework\App\ResourceConnection $resource,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
		\Magento\Framework\Message\ManagerInterface $messageManager){

    	$this->_objectManager = $objectmanager;
		$this->_resource = $resource;
		$this->_orderRepository = $orderRepository;
		$this->messageManager = $messageManager;
		
		$scopeConfig =  $this->_objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

		$this->_partnercode = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_setting/invitionprintshipm2_partnercode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);		
		$this->_apikey = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_setting/invitionprintshipm2_apikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);;				
		$this->_env = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_setting/invitionprintshipm2_version', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);					
		
		$this->_invitionsku = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/skuattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);		
	 	$this->_statusordercommitted =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_orderstatussettings/statusordernaversturen', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);	
		
		$this->_defaultshippingmethod =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_shipping/defaultshipping', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);	
		
		try {
			$this->_shippingmethodsettings = unserialize($scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_shipping/shippingmethods', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
		} catch (\Exception $e) {
		//var_dump($e);
		}
		
		if ($this->_partnercode == NULL) { $this->_partnercode = ""; }
		if ($this->_apikey == NULL) { $this->_apikey = ""; }
		if ($this->_env == NULL) { $this->_env = ""; }
		
		$this->_client = new \Invition\Partnerclient\Client($this->_partnercode, $this->_apikey, $this->_env);		
    }
	
	/**
	 * getStockList() Get a stock list from Invition through the Partnerclient
	 * 
	 * @return array
	 */
	function getStockList() {
		try {
			return $this->_client->getStockList();
		} catch (\Exception $e) {
			$this->messageManager->addErrorMessage(__($e->getMessage()));
		}
	}
	 
	
	/** 
	 * submitOrder() Submit an order to Invition
	 */
	function submitOrder($order) {
		 
		try {
			$shippingaddress = $order->getShippingAddress();
		
			// Creating an order requires a destination address
			$dest = new \Invition\Partnerclient\DestinationAddress();
		
			$dest->firstname = $shippingaddress->getData('firstname');
			$dest->lastname =  $shippingaddress->getData('lastname');
			$dest->streetname =  $shippingaddress->getData('street');
			$dest->housenumber = "";
			$dest->housenumberAddition = "";
			$dest->zipcode =  $shippingaddress->getData('postcode');
			$dest->city = $shippingaddress->getData('city');
			$dest->countryCode =$shippingaddress->getData('country_id');
			$dest->company = $shippingaddress->getData('company');
			
			// Create a new order and set the destination address we just created
			$myOrder = $this->_client->newOrder();
			$myOrder->setDestinationAddress($dest);
	
			
			$orderLines = $order->getAllItems();
			
			
			foreach($orderLines  as $orderLine) { 
				$productId = $orderLine->getProductId();
				$productModel = $this->_objectManager->create('\Magento\Catalog\Model\Product');
				$product = $productModel->load($productId);
			
				$invitionsku = $product->getData($this->_invitionsku);
				if (strlen($invitionsku) > 0) {
					$quantity = (int)$orderLine->getQtyOrdered();
					$itemSKU = $invitionsku;

					$productOptions = $orderLine->getProductOptions();
					if (array_key_exists( "downloadedproductionfile", $productOptions)) {
					$productionfile = $productOptions["downloadedproductionfile"];

					if (file_exists($productionfile)) {

						$imageBytes = file_get_contents($productionfile, true);
						$imageReference = $this->_client->uploadImage($imageBytes);

					}
					
					// Lets add a single order line to the order, and set a print image on that line. There is no limit on the number of order lines.
					$line = $myOrder->addLine($itemSKU, $quantity);

					// For this personalized item, we use the image reference that we got after uploading the image earlier.
					if (file_exists($productionfile)) {
						$line->setImageReference($imageReference);
					}
				}
				}
				
			}
			
			$myOrder->setShippingMethod(intval($this->getShippingMethodId($order)));
			$partnerReference = $order->getIncrementId();
			$myOrder->setPartnerReference($partnerReference);

			// At this point we actually send the order through the Invition API
			$error = $myOrder->execute();
			
			$ref = $myOrder->getReference();
			$this->orderSetReference($order, $ref );

		} catch (\Exception $e) {

			$error = $e->getMessage();
			
			$history = $order->addStatusHistoryComment('Commit error: ' . $error, false);
			$history->setIsCustomerNotified(false);
 
			$order->save();
			
			$this->createAdminNotification($order->getIncrementId(), $error);
			
		}	
	}
	
	/**
	 * createAdminNotification() Drop a notification of failed order in Magento admin inbox
	 */
	function createAdminNotification($ordernumber, $error) {
		$title = "Commit errror: $ordernumber";
		$description = "Error while committing order. $error";
		
		$deploymentConfig = $this->_objectManager->get('Magento\Framework\App\DeploymentConfig');

		$prefix = $deploymentConfig->get('db/table_prefix');	
		 
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		 	
		$title 			= $connection->quote($title);	
		$description 	= $connection->quote($description);
		
			
		$sql = "INSERT INTO `" . $prefix . "adminnotification_inbox` (
			`severity`,
			`date_added`,
			`title`,
			`description`,
			`url`,
			`is_read`,
			`is_remove`
		) VALUES (
			1,
			'" . date('Y-m-d H:i:s') . "',
			" . $title . ",
			" . $description . ",
			'',
			0,
			0);";
		$connection->query($sql );
  
	}
	
	/**
	 * orderSetReference() When an order was committed succesfully to Invition, add the Invition reference to the order
	 */
	function orderSetReference($order, $ref) {
		 
		$deploymentConfig = $this->_objectManager->get('Magento\Framework\App\DeploymentConfig');

		$prefix = $deploymentConfig->get('db/table_prefix');	
		
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$sql = "update " . $prefix . "sales_order_grid set invitionref='" . $ref  . "' where entity_id=" . $order->getId();
		
		$connection->query($sql );

        $sql = "update " . $prefix . "sales_order set invitionref='" . $ref  . "' where entity_id=" . $order->getId();

        $connection->query($sql );
		
		$order = $this->_orderRepository->get($order->getId());;
		
		$history = $order->addStatusHistoryComment('Order ref: ' . $ref, false);
		$history->setIsCustomerNotified(false);

		$order->setData('state', 'processing');
		$order->setStatus($this->_statusordercommitted);    	
		$order->save();	

	}
	
	/**
	 * getOrderInfo() Fetch order info from Invition
	 * 
	 * @return array
	 */
	function getOrderInfo($ref) {
		$orderInfo = $this->_client->getOrder($ref);
		return $orderInfo;		
	}
	
	/**
	 * Fetch shipment methods from Invition
	 * 
	 * @return array
	 */
	public function getShippingMethods() {
		if ($this->_env != "" && $this->_apikey != "" && $this->_partnercode != "") {
			try {
				return $this->_client->getShippingMethods();
			} catch (\Exception $e) {
				$this->messageManager->addErrorMessage(__($e->getMessage()));
			}
		} else {
			return array();
		}
	}	
	
	/**
	 * Search the shipping mapping for the right Invition Shipping method ID
	 * 
	 * @return int
	 */
	public function getShippingMethodId($order) {
		$shippingmethod = $order->getShippingMethod();
		$shippingmethod_id = $this->_defaultshippingmethod;
		
		foreach ($this->_shippingmethodsettings as $item) 
		{	
			if (strpos(strtoupper($shippingmethod), strtoupper($item["shippingmethod"])) !== false ) {
				$shippingmethod_id = $item["apishippingmethod"];
			}
		}
		
		return $shippingmethod_id;
	}
}
