<?php

namespace Invition\InvitionPrintShipM2\Model\System\Config;

/**
 * Price types mode source
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Taxclasses extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Magento\Eav\Model\EntityFactory
     */
    protected $eavEntityFactory;

    public function __construct(
        \Magento\Eav\Model\EntityFactory $eavEntityFactory
    ) {
        $this->eavEntityFactory = $eavEntityFactory;
    }
	
    public function getAllOptions()
    {
	   	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$producttype = $objectManager->create('\Magento\Sales\Model\Order\Tax');
		return $producttype->getAllOptions();
    }
 
    public function toOptionArray()
    {
       	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$producttype = $objectManager->create('\Magento\Sales\Model\Order\Tax');
		return $producttype->getOptionArray();
    }
	 
}
