<?php 

namespace Invition\InvitionPrintShipM2\Model;

use Invition\InvitionPrintShipM2\Api\Data\ItemsInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Invition;

class Orderupdater extends \Magento\Framework\Model\AbstractModel 
{
 	private $_objectManager;
 	private $_productFactory;	
 	private $_productAttributeRepository;
 	private $_productRepository;
 	protected $_resource;

 	private $_statusreadyforcommit;
 	private $_statusordercommitted;
 	private $_invitionsku;							
	private $_orderCollectionFactory;
    private $_transactionFactory;
    private $_trackFactory;
	private $_orderRepository;
	
    /**
     * Initialize resource model
     *
     * @return void
     */
   	public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Catalog\Model\ProductFactory $productFactory,		
		\Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Framework\App\ResourceConnection $resource,		
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository
)
	{
    	$this->_objectManager = $objectmanager;
		$this->_productFactory = $productFactory;
		$this->_productAttributeRepository = $productAttributeRepository;
		$this->_productRepository = $productRepository;				
		$this->_resource = $resource;
		$this->_orderCollectionFactory = $orderCollectionFactory;
		$this->_transactionFactory = $transactionFactory;
        $this->_trackFactory = $trackFactory;
		$this->_orderRepository = $orderRepository;
		
		$scopeConfig =  $this->_objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

	 	$this->_statusreadyforcommit =   $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_orderstatussettings/statusordernadownload', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$this->_statusordercommitted =   $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_orderstatussettings/statusordernaversturen', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$this->_invitionsku =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/skuattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);		
	 	
    } 
	 
	/**
	 * Walk through all orders with the right order status to commit to Invition
	 * 
	 * @return void
	 */
	public function checkForOrdersToSubmit() {
		
		$orders = $this->_orderCollectionFactory->create()->addAttributeToSelect('*')->addFieldToFilter('status', $this->_statusreadyforcommit);
		
		foreach ($orders as $order) {
			if ($this->orderIsInvitionOrder($order)) {
				echo "Submitting order " . $order->getIncrementId() . " to Invition\r\n";
				
				$this->submitOrder($order);
			}		
		}
	}
	
	/**
	 * Check if an order contains Invition products
	 * 
	 * @param object 
	 * @return bool
	 */
	public function orderIsInvitionOrder($order) { 
		$result = false;
		
		$orderLines = $order->getAllItems();
		
		foreach($orderLines as $orderLine) {
			$productId = $orderLine->getProductId();
			
			$productModel = $this->_objectManager->create('\Magento\Catalog\Model\Product');
			$product = $productModel->load($productId);
		
			$invitionSku = $product->getData($this->_invitionsku);
	
			if (strlen($invitionSku) > 0) {
				$result = true;
				break;
			}
		}
		
		return $result;
	}
	
	/**
	 * Submit an order to the Invition API
	 * 
	 * @param $order Magento order object
	 * @return void
	 */
	public function submitOrder($order) {
		
		if (strlen($order->getData('invitionref')) > 0) {
			$order = $this->_orderRepository->get($order->getId());
			
			$order->setData('state', 'processing');
			$order->setStatus($this->_statusordercommitted);    	
			$order->save();	
			
		} else {
		
			$client = $this->_objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
			$client->submitOrder($order);
		}
	}
	
	/**
	 * Update order status to given reference
	 * 
	 * @param string $ref Invition reference
	 */
	public function updateOrderstatuses($ref = "") {
		$orders = $this->_orderCollectionFactory->create()->addAttributeToSelect('*')->addFieldToFilter('status', $this->_statusordercommitted); 
		
		foreach ($orders as $order) {
			if ($this->orderIsInvitionOrder($order)) {
				$this->updateOrderStatus($order, $ref);
			}
		}
	}
	
	/**
	 * Set order status to complete
	 * 
	 * @param object Magento order object
	 * @return void
	 */
	public function setComplete($order) {
		$history = $order->addStatusHistoryComment('Uw order is verwerkt.', true);
		$history->setIsCustomerNotified(true);
		$order->setData('state', "complete");
		$order->setStatus("complete");    	
		$order->save();
	}
	

	/**
	 * Check for changes on an order and create a shipment if possible
	 * 
	 * @param object $order Magento order object
	 * @param string $ref Invition order reference
	 * @return void
	 */
	public function updateOrderStatus($order, $ref = "") {
		$client = $this->_objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		
		// If an Invition reference was given, check if it belongs to the order
		$docheck = true;
		if (isset($ref) && $ref != "") {
			$docheck = false;
			
			if ($ref == $order->getData('invitionref'))
			{
				$docheck = true;	
			}
		}
		
		if ($docheck) {
		
			$apiOrderInfo = $client->getOrderInfo($order->getData('invitionref'));
		
			// The order is completely finished
			if ($apiOrderInfo->status == "closed") {

				$status = $apiOrderInfo->status;
				
				$orderLines = $order->getAllItems();
				$apiOrderLines = $apiOrderInfo->lines;
				$qty=array();
					
				$convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
				$shipment = $convertOrder->toShipment($order);
			
				// Walk through order lines and check for shipments
				foreach ($orderLines as $orderLine){
					$productId = $orderLine->getProductId();
					
					$productModel = $this->_objectManager->create('\Magento\Catalog\Model\Product');
					$product = $productModel->load($productId);
			
					$invitionSku = $product->getData($this->_invitionsku);
					
					$qtyShipped = $orderLine->getQtyOrdered();// $apiOrderLine->shippedQuantity;

					// Create shipment item with qty
					$shipmentItem = $convertOrder->itemToShipmentItem($orderLine)->setQty($qtyShipped);

					// Add shipment item to shipment
					$shipment->addItem($shipmentItem);
				}
				
				$email=true;
				$includeComment=false;
				$comment="";
				$trackingCode = "";
				$trackingUrl = "";			
				$shipper = "";	
				
				foreach($apiOrderInfo->shipments as $apishipment) {
					$shipper = $apishipment->shipper;
					$trackingCode = $apishipment->trackAndTraceCode;
					$trackingUrl = $apishipment->trackAndTraceURL;
				}

				if ($shipment) {

					$shipment->register();

					try {
						$shipment->save();
					
						$order->setIsInProcess(true);

						if (strlen($trackingCode) > 0) {

							$track = $this->_trackFactory->create();

							$track->setNumber($trackingUrl);
							$track->setCarrierCode($shipper);
							$track->setTitle($shipper);
							$track->setShipment($shipment);
							$track->setOrderId($shipment->getData("order_id"));
							$track->save();

						}

						$saveTransaction = $this->_transactionFactory->create();
						$saveTransaction->addObject($shipment);
						$saveTransaction->addObject($shipment->getOrder());

						$saveTransaction->save();

						$this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')->notify($shipment);

					} catch (\Exception $e) {
						
					}
				}		
			
				$this->setComplete($order);
			}
		}
	}
}
