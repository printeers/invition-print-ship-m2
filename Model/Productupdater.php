<?php 

namespace Invition\InvitionPrintShipM2\Model;

use Invition\InvitionPrintShipM2\Api\Data\ItemsInterface;
use Magento\Framework\DataObject\IdentityInterface;
use \Invition\Partnerclient;

class Productupdater extends \Magento\Framework\Model\AbstractModel 
{
 	private $_objectManager;
	private $_invitionsku;
	private $_updatestockattribute;
	private $_attributeset;	
	private $_category;		
	private $_producttype;
	private $_width;
	private $_height;				
	private $_taxclass;
	private $_updatestock;
	
	private $_productFactory;
	private $_productAttributeRepository;
	private $_productRepository;				
	protected $_resource;
		
    /**
     * Initialize resource model
     *
     * @return void
     */
   	public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Catalog\Model\ProductFactory $productFactory,		
		\Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Framework\App\ResourceConnection $resource) 
	{
    	$this->_objectManager = $objectmanager;
		$this->_productFactory = $productFactory;
		$this->_productAttributeRepository = $productAttributeRepository;
		$this->_productRepository = $productRepository;				
		$this->_resource = $resource;
		
		$scopeConfig =  $this->_objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

		$this->_invitionsku = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/skuattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);				
	 	$this->_updatestockattribute = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/updatestockattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);						
		$this->_attributeset = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/attributeset', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);		
		$this->_category =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/category', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);				
		$this->_producttype =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/producttype', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);				
		$this->_width =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/widthattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);			
		$this->_height =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/heightattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);					
		$this->_taxclass =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/taxclass', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$this->_updatestock =  $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/updatestockattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
	
	/**
	 * Create a new product with the given information
	 * 
	 * @param object $stockItem an object from Invition with the product details
	 * @return void
	 */
    public function createProduct($stockItem) {

		$product = $this->_productFactory->create();	
		$product->setName($stockItem->name);
		$product->setTypeId($this->_producttype);
		$product->setVisibility(4);
		$product->setAttributeSetId($this->_attributeset); // Default attribute set for products
		$product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
		$product->setCreatedAt(strtotime('now'));
		$product->setSku($stockItem->sku);
		$product->setWeight(0.0000);
		$product->setTaxClassId($this->_taxclass);
		$product->setMetaTitle($stockItem->name);
		$product->setPrice(0);		

		$qty=0;
		if(!empty($stockItem->availability->amountLeft)){
			$qty = $stockItem->availability->amountLeft; // Real qty is available
		} elseif($stockItem->availability->status != 'out-of-stock'){
			$qty = 999; // More then 10 available but no real qty given
		}

		$product->setStockData(
			array(
			'use_config_manage_stock' => 0, 
			'manage_stock' => 1, // Manage stock
			'min_sale_qty' => 1, // Shopping Cart Minimum Qty Allowed 
			'is_in_stock' => $stockItem->availability->status != 'out-of-stock', // Stock Availability of product
			'qty' => $qty // qty of product in stock
			)
		);
		$product->setCategoryIds(array($this->_category));
		$product->setData($this->_invitionsku , $stockItem->sku);
		$product->setData($this->_width , $stockItem->dimensionWidthMM);
		$product->setData($this->_height , $stockItem->dimensionHeightMM);
		$product->setData($this->_updatestock , 1);
		
		$this->_productRepository->save($product);
		
	}
	
	/**
	 * Get all items from Invition and update changed information
	 * 
	 * @return void
	 */
	public function updateProducts() {
		$client = $this->_objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		$stockItems = $client->getStockList()->items;
	
    	$deploymentConfig = $this->_objectManager->get('Magento\Framework\App\DeploymentConfig');

		$tableprefix = $deploymentConfig->get('db/table_prefix');
		
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$query = "
			SELECT 
				distinct value, 
				entity_id 
			FROM 
				".$tableprefix."catalog_product_entity_varchar 
			INNER JOIN 
				".$tableprefix."eav_attribute 
				ON ".$tableprefix."eav_attribute.attribute_id=".$tableprefix."catalog_product_entity_varchar.attribute_id 
			WHERE 
				attribute_code='".$this->_invitionsku."' AND
				NOT value IS NULL AND 
				value <> '' AND 
				EXISTS (
					SELECT 
						value 
					FROM 
						".$tableprefix."catalog_product_entity_int 
					INNER JOIN 
						".$tableprefix."eav_attribute 
						ON ".$tableprefix."eav_attribute.attribute_id=".$tableprefix."catalog_product_entity_int.attribute_id 
					WHERE 
						attribute_code='" .$this->_updatestockattribute . "' AND 
						".$tableprefix."catalog_product_entity_int.entity_id = ".$tableprefix."catalog_product_entity_varchar.entity_id AND
						".$tableprefix."catalog_product_entity_int.value=1
				)
			ORDER BY entity_id desc";
		$invitionitems = $connection->fetchAll($query );

		foreach($invitionitems as $invitionItem) {
			foreach ($stockItems as $stockItem) {
				if ($stockItem->sku == $invitionItem["value"]) {

					$this->updateProduct($stockItem, $invitionItem["entity_id"]);
										
					break;
				}
			}
		} 
	}
	
	/**
	 * Update Magento product when new information is given from API
	 * 
	 * @param object $stockItem an object from Invition with the product details
	 * @param int $entityId the Magento product ID
	 * @return void
	 */
	public function updateProduct($stockItem, $entityId) {

		 $productModel = $this->_objectManager->create('\Magento\Catalog\Model\Product');
		 $product = $productModel->load($entityId);
		
		 if (($product->getData($this->_updatestockattribute) != null) && $product->getData($this->_updatestockattribute) == "1"){
			
			$isinstock = $stockItem->availability->status != 'out-of-stock';
			if (!$isinstock && intval($stockItem->availability->canBackorder) == 1){
				$isinstock = true;
			}
			
			$qty=0;
			if(!empty($stockItem->availability->amountLeft)){
				$qty = $stockItem->availability->amountLeft; // Real qty is available
			} elseif($stockItem->availability->status != 'out-of-stock'){
				$qty = 999; // More then 10 available but no real qty given
			}

			$isinstock = ($isinstock ? 1 : 0);
			
			$deploymentConfig = $this->_objectManager->get('Magento\Framework\App\DeploymentConfig');

			$tableprefix = $deploymentConfig->get('db/table_prefix');
			$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		
			$query = "
				UPDATE 
					".$tableprefix."cataloginventory_stock_item 
				SET 
					qty=" . $qty . ", 
					is_in_stock=" . $isinstock . " 
				WHERE 
					product_id=" . $entityId;
			$connection->query($query);	
		
		}
		
		$needsave = false;
		
		if ($product->getData($this->_invitionsku) != $stockItem->sku) {
			$product->setData($this->_invitionsku , $stockItem->sku);
			$needsave = true;			
		}

		if ($product->getData($this->_width) != $stockItem->dimensionWidthMM) {
			$product->setData($this->_width , $stockItem->dimensionWidthMM);
			$needsave = true;			
		}
		if ($product->getData($this->_height) != $stockItem->dimensionHeightMM) {
			$product->setData($this->_height , $stockItem->dimensionHeightMM);
			$needsave = true;			
		}
		
		if ($needsave) { 
			$product->save();
		}
		
	}
     
}
