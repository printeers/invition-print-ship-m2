<?php 
namespace Invition\InvitionPrintShipM2\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
  private $eavSetupFactory;
 
 public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
 
 }
 
 public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) {
  	$setup->startSetup();
 
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
 
        /**
         * Order attributes:
         * 
         * invitionref - Reference to order at Invition
         */

        /*  */
        $eavSetup->addAttribute(
            \Magento\Sales\Model\Order::ENTITY,
            'invitionref',
            [
                'type' => 'varchar',
                'input' => 'text',
                'label' => 'invitionref',
                'required' => false,
                'user_defined' => true,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
               
                 
            ]
        );

        /**
         * Order statusses
         * 
         * Images downloaded - Status to be used when imageprovider added the images to the order
         * Ordered at Invition - Status to be used when order was placed at Invition
         */
        $data[] = ['status' => 'images_downloaded', 'label' => 'Images downloaded'];
        $data[] = ['status' => 'order_committed', 'label' => 'Order committed'];
        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);
 
        $setup->getConnection()->insertArray(
        $setup->getTable('sales_order_status_state'),
        ['status', 'state', 'is_default','visible_on_front'],
        [
            ['images_downloaded','processing', '0', '1'], 
            ['order_committed', 'processing', '0', '1']
        ]
        );
        
        
        /**
         * Product attributes
         * 
         * Invition SKU - This SKU connects a Magento product to an Invition product
         * Update stock - Will the stock be overwritten by Invition?
         * Design width - Width of the print image in mm
         * Design height - Height of the print image in mm
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'invition_sku',
            [
                'group' => 'Invition Print & Ship',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Invition SKU',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => ''
            ]
        );
 
         $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'invition_update_stock',
            [
                'group' => 'Invition Print & Ship',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Automatic stock management',
                'input' => 'boolean',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => ''
            ]
        );
 
         $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'invition_width_mm',
            [
                'group' => 'Invition Print & Ship',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Design width in mm',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => ''
            ]
        );
 
         $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'invition_height_mm',
            [
                'group' => 'Invition Print & Ship',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Design height in mm',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => ''
            ]
        );

        $setup->endSetup();
 	}
}
