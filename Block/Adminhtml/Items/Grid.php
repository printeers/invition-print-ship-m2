<?php

 namespace Invition\InvitionPrintShipM2\Block\Adminhtml\Items;

 use Magento\Backend\Block\Widget\Grid\Extended;
 use Magento\Backend\Block\Template\Context;
 use Magento\Backend\Helper\Data;
 use Invition\InvitionPrintShipM2\Model\ResourceModel\Items\Collection; 
 use Magento\Framework\DataObject;

 class Grid extends Extended
 {
	protected $_resource;

    /**
     * @var Collection
     */
    protected $_collection;

	private $_invitionsku;

   /**
    * Constructor
    *
    * @param Context $context
    * @param Data $backendHelper
    * @param Collection $collection
    * @param array $data
    */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Collection $collection,
        array $data = [],
		\Magento\Framework\App\ResourceConnection $resource
    ) {
		$this->_resource = $resource;
        $this->_collection = $collection;

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();		
		$scopeConfig =  $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

		$this->_invitionsku = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/skuattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);				
	
        parent::__construct($context, $backendHelper, $data);
    }


   /**
    * @return void
    */
    protected function _construct()
    {
	    parent::_construct();
        $this->setId('items_grid');
        $this->setDefaultSort('sku');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('items_record');
    }

    protected function _prepareCollection()
    {
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$model = $objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		$collection = $this->_collection;
		
		$stockItems = $model->getStockList()->items;

		$deploymentConfig = $objectManager->get('Magento\Framework\App\DeploymentConfig');

		$tableprefix = $deploymentConfig->get('db/table_prefix');
				
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$query = "SELECT distinct value FROM ".$tableprefix."catalog_product_entity_varchar inner join ".$tableprefix."eav_attribute on ".$tableprefix."eav_attribute.attribute_id=".$tableprefix."catalog_product_entity_varchar.attribute_id where attribute_code='".$this->_invitionsku."' and NOT value IS NULL AND value <> ''";
		$invitionitems = $connection->fetchCol($query );

		foreach ($stockItems as $data) {
			if (!in_array($data->sku, $invitionitems)) {
				$obj = new \Magento\Framework\DataObject();
				$obj->setItemid($data->id);
				$obj->setSku($data->sku);
				$obj->setName($data->name);			
				$obj->setWidth($data->dimensionWidthMM);	
				$obj->setHeight($data->dimensionHeightMM);						
				$obj->setStatus($data->availability->status);						
				$obj->setBackorder($data->availability->canBackorder);									
				$obj->setAmountleft($data->availability->amountLeft);												
						
				$collection->addItem($obj);
			}
		}

		$this->setCollection($collection);
		return parent::_prepareCollection();

	}
 
	 protected function _prepareMassaction()
	{
		$this->setMassactionIdField('itemid');
		$this->getMassactionBlock()->setFormFieldName('massaction');
	
		$this->getMassactionBlock()->addItem(
			'addtoproducts',
			[
				'label' => __('Add To Products'),
				'url' => $this->getUrl('invitionprintshipm2/items/addtoproducts')
			]
		);

		$this->getMassactionBlock()->addItem(
			'connecttoproducts',
			[
				'label' => __('Connect To Existing Products'),
				'url' => $this->getUrl('invitionprintshipm2/items/connecttoproducts')
			]
		);
		
		return $this;
	}

    protected function _prepareColumns()
    {
		 $this->addColumn(
			'itemid',
			[
				'header' => __('item id'),
				'sortable' => true,
				'index' => 'itemid',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
	  $this->addColumn(
			'sku',
			[
				'header' => __('Sku'),
				'sortable' => true,
				'index' => 'sku',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
		$this->addColumn(
			'name',
			[
				'header' => __('name'),
				'sortable' => true,
				'index' => 'name',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
		$this->addColumn(
			'width',
			[
				'header' => __('width'),
				'sortable' => true,
				'index' => 'width',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
		$this->addColumn(
			'height',
			[
				'header' => __('height'),
				'sortable' => true,
				'index' => 'height',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
		$this->addColumn(
			'status',
			[
				'header' => __('status'),
				'sortable' => true,
				'index' => 'status',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
		$this->addColumn(
			'backorder',
			[
				'header' => __('can backorder'),
				'sortable' => true,
				'index' => 'backorder',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
		$this->addColumn(
			'amountleft',
			[
				'header' => __('amount left'),
				'sortable' => true,
				'index' => 'amountleft',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id'
			]
		);
		
       return parent::_prepareColumns();
    }
}
