<?php

 namespace Invition\InvitionPrintShipM2\Block\Adminhtml\Items\Edit;

/**
 * Adminhtml blog post edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    protected $_systemStore;
	protected $_resource;
	protected $_invitionsku;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = [],
		\Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_systemStore = $systemStore;
		$this->_resource = $resource;
	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();		
		$scopeConfig =  $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

		$this->_invitionsku = $scopeConfig->getValue('invition_invitionprintshipm2/invitionprintshipm2_attributesettings/skuattribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);				
	
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
       
    }

	private function getValue($productoptions, $sku) {
		$result = "";
 
		foreach($productoptions as $option) {
			 
			
			if (strtoupper($option) == strtoupper($sku)) {
				$result =   array_search ($option, $productoptions);  
			}
		}
		return $result;
	}
	
    /**
     * Create the form for 'Add products'
     *
     * @return $this
     */
    protected function _prepareForm()
    {
	
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form',
				'action' => $this->getData('action'), 
				'method' => 'post',
				'enctype' => 'multipart/form-data']
			]
        );

        $form->setHtmlIdPrefix('item_');
 
        $fieldset = $form->addFieldset(
				'basefieldset',
				['legend' => __(''), 'class' => 'fieldset-wide']
			);

		$dataIds = isset($_POST["massaction"]) ? explode(",", $_POST["massaction"]) : null;
		if ($dataIds == null) {
			$dataIds = explode(",", $this->_request->getParam('massaction'));
			
		}

		$stockitems = $this->getStockItemsByArray($dataIds);
		$products = $this->getProducts();
		
		$productoptions = array();

		$productoptions[-1] = 'Please select item(s)';
		foreach($products as $product) {
			$productoptions[$product["value"]] = $product["label"];
		}
		
		$fieldset->addField(
				'itemIds',
				'hidden',
				 [			 
					'name' => "itemIds",					 
					'value' => implode(";",$dataIds)
				]          
			);
		
		foreach($stockitems as $stockitem) { 
			$fieldset->addField(
				$stockitem->id . '_product',
				'select',
				 [
					'label' => $stockitem->sku . '-' . $stockitem->name,
					'title' => $stockitem->sku . '-' . $stockitem->name,
					'name' => $stockitem->id . '_product',					 
					'options' => $productoptions,
					'value' => $this->getValue($productoptions, $stockitem->sku)
				]          
			);
		
		}
		  
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
	
	/**
	 * Get a set of Invition products not connected to a Magento product yet
	 * 
	 * @return array
	 */
	protected function getProducts() {
		$result = array();

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$deploymentConfig = $objectManager->get('Magento\Framework\App\DeploymentConfig');
		$tableprefix = $deploymentConfig->get('db/table_prefix');
		
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$query = "
			SELECT 
				`e`.entity_id, 
				`e`.`sku` AS `sku` 
			FROM 
				`" .$tableprefix . "catalog_product_entity` AS `e` 
			LEFT JOIN 
				`" .$tableprefix . "catalog_product_entity_varchar` AS `at_invition_sku` 
				ON (`at_invition_sku`.`entity_id` = `e`.`entity_id`) AND (
				`at_invition_sku`.`attribute_id` = (
					SELECT 
						attribute_id 
					FROM 
						eav_attribute 
					WHERE 
						attribute_code='" .$this->_invitionsku . "'
					)
				) 
			WHERE 
				at_invition_sku.value IS NULL AND
				NOT e.sku IS NULL AND
				NOT e.sku = ''";
		$products = $connection->fetchAll($query );
		
		foreach($products as $product) {
			$item = array();
			
			$item["label"] = (string)$product["sku"];
			$item["value"] = (string)$product["entity_id"];
			
			array_push($result, $item);
		}
		
		return $result;
	}
	
	/**
	 * Get all the Invition stock items which are in the supplied array
	 * 
	 * @param array $dataIds contains a set of product IDs
	 * @return array
	 */	
	public function getStockItemsByArray($dataIds) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$client = $objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		$stockItems = $client->getStockList()->items;
 
		$result = array();
		foreach($stockItems as $stockItem) {
			if (in_array($stockItem->id, $dataIds)) {
				array_push($result, $stockItem);
			}
		}	
		return $result;
	}
}
