<?php
namespace Invition\InvitionPrintShipM2\Block\Adminhtml\System\Config\Form\Field;

class Shippingmethods extends \Invition\InvitionPrintShipM2\Model\Config\Backend\Serialized\ArraySerialized
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $_elementFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        array $data = []
    )
    {
        $this->_elementFactory  = $elementFactory;
        parent::__construct($context,$data);
    }
    protected function _construct()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$apishippingmethods = $objectManager->create('Invition\InvitionPrintShipM2\Model\System\Config\Apishippingmethods');
		$apishippingmethods = $apishippingmethods->toOptionHash();
			
		$storeshippingmethods = $objectManager->create('Invition\InvitionPrintShipM2\Model\System\Config\Storeshippingmethods');
		$storeshippingmethods = $storeshippingmethods->toOptionHash();
	
			
		$this->addColumn('apishippingmethod', ['label' => __('API shipping method'), 'type' => 'select', 'options'=> $apishippingmethods]);
        $this->addColumn('shippingmethod', ['label' => __('Store shipping method'), 'type' => 'select', 'options'=> $storeshippingmethods]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

}
