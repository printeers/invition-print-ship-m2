## Changelog
This document describes changes made to the Invition Print & Ship API client for Magento 2 over time.

### 2018-10-09 15:30
- Fixed cronjobs and improved output
- Added check to getStockList and getShippingMethods to prevent admin from crashing when API details are incorrect
- Fixed: When stock is 0, stock is imported as 999
- Fixed: Import real stock value above 10pcs when allowed
- Fixed: Adding or connecting multiple products only adds the first product
- Changed: Update stock set as true when adding new product
- Rewritten queries for readability

### 2018-10-01 16:00
- Error message is displayed when entering wrong API details

### 2018-09-17 14:00
From now on changes will be managed here and published to Packagist.