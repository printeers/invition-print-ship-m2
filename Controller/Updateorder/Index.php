<?php namespace Invition\InvitionPrintShipM2\Controller\Updateorder;

use Magento\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$model = $objectManager->create('Invition\InvitionPrintShipM2\Model\Orderupdater');
		$model->updateOrderstatuses((isset($_GET["order_reference"])  ? $_GET["order_reference"] : ""));	
	}

    /**
     * Is the user allowed to view the blog post grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Invition_InvitionPrintShipM2::items');
    }

}
