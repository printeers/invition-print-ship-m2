<?php 

namespace Invition\InvitionPrintShipM2\Controller\Adminhtml\Items;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
		
	 
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

     	$itemIds = explode(";", $_POST["itemIds"]);
		$stockItems = $this->getStockItemsByArray($itemIds); 		 		

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productUpdater = $objectManager->create('Invition\InvitionPrintShipM2\Model\Productupdater');
				
		foreach($stockItems as $stockItem) {
			$selectedId = $_POST[$stockItem->id . "_product"];

			if ((int)$selectedId != -1) {
				$productUpdater->updateProduct($stockItem, (int)$selectedId);
			}	
		}	
				
		$resultRedirect = $this->resultRedirectFactory->create();				
		return $resultRedirect->setPath('*/*/');	     
    }

    /**
     * Is the user allowed to view the blog post grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Invition_InvitionPrintShipM2::items');
    }
	
    /**
     * Get an specific set of Invition products 
     * 
     * @param array $dataIds a set of IDs to be retrieved as Invition product
     * @return array
     */
	public function getStockItemsByArray($dataIds) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	
		$client = $objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
        $stockItems = $client->getStockList()->items;
 
		$result = array();
		foreach($stockItems as $stockItem) {
			if (in_array($stockItem->id, $dataIds)) {
				array_push($result, $stockItem);
			}
        }	

		return $result;
	}
}
