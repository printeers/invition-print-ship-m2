<?php namespace Invition\InvitionPrintShipM2\Controller\Adminhtml\Items;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Addtoproducts extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
		
	 
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

	public function itemExists($stockItem) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$productRepository  = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
		
		try {
			$product = $productRepository->get($stockItem->sku);
		} catch (\Exception $e) {
			$product = false;
		}	
		
		if ($product) {
   			return true;
		}	 else {
			return false;
		}
	}
	
    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Invition_InvitionPrintShipM2::items');
        $resultPage->addBreadcrumb(__('InvitionPrintShipM2'), __('InvitionPrintShipM2'));
        $resultPage->addBreadcrumb(__('Beheer producten'), __('Add to Products'));
        $resultPage->getConfig()->getTitle()->prepend(__('Add to Products'));

		$dataIds = explode(",", $_POST["massaction"]);

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 
		$client = $objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		$stockItems = $client->getStockList()->items;
			
		$existingitems = array();
		
		foreach($dataIds as $dataId) {
			foreach($stockItems as $stockItem) {
				if ($stockItem->id == $dataId) {
					if (!$this->itemExists($stockItem)) {
						$this->createProduct($stockItem);
					} else {
						array_push($existingitems, $stockItem->id);
					} 
				
					break;
				}
			}					
		}   
		
		$resultRedirect = $this->resultRedirectFactory->create();				
			
		if (sizeof($existingitems) == 0) { 
			return $resultRedirect->setPath('*/*/');		        
    	} else {
			$this->_redirect('*/*/Connecttoproducts', array('massaction' => implode(";", $existingitems)));
		}
	}

    /**
     * Is the user allowed to view the blog post grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Invition_InvitionPrintShipM2::items');
    }

	
	/**
	* Create the productupdater class and creates the product
	*/	
	public function createProduct($stockItem) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productUpdater = $objectManager->create('Invition\InvitionPrintShipM2\Model\Productupdater');
	
		return $productUpdater->createProduct($stockItem);
	}
}
