<?php

namespace Invition\InvitionPrintShipM2\Controller\Adminhtml\Items;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Connecttoproducts extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
	 
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
     	 /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Invition_InvitionPrintShipM2::items');
        $resultPage->addBreadcrumb(__('InvitionPrintShipM2'), __('InvitionPrintShipM2'));
        $resultPage->addBreadcrumb(__('Beheer producten'), __('Connect products'));
        $resultPage->getConfig()->getTitle()->prepend(__('Connect products'));

        return $resultPage;			       
    }

    /**
     * Is the user allowed to view the blog post grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Invition_InvitionPrintShipM2::items');
    }
	
	public function getStockItemsByArray($dataIds) {
		$client = $objectManager->create('Invition\InvitionPrintShipM2\Model\Clientcon');
		$stockItems = $client->getStockList()->items;
 
		$result = array();
		foreach($stockItems as $stockItem) {
			if (in_array($stockItem->id, $dataIds)) {
				array_push($result, $stockItem);
			}
		}	
		return $result;
	}
}
