<?php
namespace Invition\InvitionPrintShipM2\Plugin;

class PluginAddbuttons
{
	private $_urlBuilder;
	
	public function __construct(
    
    \Magento\Framework\UrlInterface $urlBuilder
   
) {
    
    $this->_urlBuilder = $urlBuilder;
   
}
    public function beforePushButtons(
        \Magento\Backend\Block\Widget\Button\Toolbar\Interceptor $subject,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {

        $this->_request = $context->getRequest();
        if($this->_request->getFullActionName() == 'sales_order_view'){

			$orderId = $this->_request->getParam('order_id');
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				
			$orderRepository = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface');
		
			$order = $orderRepository->get($orderId);;
		
			$invitionref = $order->getData("invitionref");
			 
			if (empty($invitionref)) {
				$orderupdater = $objectManager->create('\Invition\InvitionPrintShipM2\Model\Orderupdater');
				
				if ($orderupdater->orderIsInvitionOrder($order)) {
				
					$url = $this->_urlBuilder->getUrl('invitionprintshipm2/sendorder/', array("id"=> $orderId));

					$buttonList->add(
						'buttonsendorder',
						['label' => __('Commit order to Invition'), 'onclick' => 'setLocation(\'' . $url . '\')', 'class' => 'reset'],
						-1
					);
				}
			}
        }

    }
}